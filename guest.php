<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
  	 <style type="text/css">
  	 	* {
  	 		font-family: "STXinwei";
  	 	}
  	 </style>
</head>

<body>

<div class="jumbotron">
        <h1>欢迎使用本系统租还车辆</h1>
        <div align="right">
			<a href="rent.html">租车</a>
			&nbsp&nbsp
			<a href="return.html">还车</a>
		</div>
</div>

<div class="container">
<?php
	session_start();
	if (isset($_SESSION['uid'])==0){
		echo "<script>alert('请先登录');window.location.href='guest_login.html';</script>";
	}
	else {
		echo "你好，";
		echo $_SESSION['uid'];
	}
?>
</div>

<br />

<div class="container">
<div class="col-md-5 col-md-offset-0">

	<legend>这些是你正在租的车</legend>
	<?php 
	error_reporting(E_ALL ^ E_DEPRECATED);

	$con=mysql_connect("localhost","root","");
	if (!$con){
		die('Could not connect: '.mysql_error());
	}

	mysql_select_db("carsystem",$con);

    mysql_query("SET NAMES utf8");

	$sql="select *
	from car join rent on car.id=rent.car_id
	where guest_id='$_SESSION[uid]'";

	$result=mysql_query($sql);

	echo "<table border='1'>
	<tr>
	<th>车号</th>
	<th>品牌</th>
	<th>车名</th>
	<th>车牌号</th>
	<th>出厂年份</th>
	<th>颜色</th>
	<th>价格</th>
	<th>借车日期</th>
	<th>账单</th>
	</tr>";
	while($row = mysql_fetch_array($result))
	{
	echo "<tr>"; 
	echo "<td>" . $row['car_id'] . "</td>";
	echo "<td>" . $row['brand'] . "</td>";
	echo "<td>" . $row['name'] . "</td>";
	echo "<td>" . $row['licence'] . "</td>";
	echo "<td>" . $row['year'] . "</td>";
	echo "<td>" . $row['color'] . "</td>";
	echo "<td>" . $row['price'] . "</td>";
	echo "<td>" . $row['date'] . "</td>";
	$zero1=strtotime (date("y-m-d h:i:s"));
	$zero2=strtotime ($row['date']);
	$duration=ceil(($zero1-$zero2)/86400); //60s*60min*24h
	//echo "<td>" . $duration . "</td>";
	echo "<td>" . $row['price']*$duration . "</td>";
	echo "</tr>";
	}
	echo "</table>";
	mysql_close($con);


	?>
    <br/>
    <h2><a href="index.html">返回主页</a></h2>
</div>

</div>


<br />



</body>
</html>
